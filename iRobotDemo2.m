function  HW1(serPort)

  tStart= tic;                                        % Time limit marker
  maxDuration = 40;                                   % 20 seconds of max duration time    
 
  while toc(tStart) < maxDuration
      [x y] = OverheadLocalizationCreate(serPort);
      display(x);
      display(y);
      SetFwdVelRadiusRoomba(serPort,0.1,inf);
      pause(0.1);
  end  
  SetFwdVelRadiusRoomba(serPort, 0, 2);                                       % Stop the Robot

end