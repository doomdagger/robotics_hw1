
% file containing map information
% formatting:
% wall x1 y1 x2 y2
%   order does not matter between the points
% line x1 y1 x2 y2
% beacon x y [r g b] id_tag
%   [r g b] is the red-green-blue color vector
% virtwall x y theta
%   virtual walls emit from a location, not like real walls
%   theta is the angle relative to the positive x-axis
wall -1.440 3.190 -1.474 0.170
wall -1.474 0.170 0.267 0.350
wall 0.284 0.370 0.233 3.030
wall -1.457 3.170 0.233 3.030
wall 0.129 -1.650 0.129 -2.530
wall 0.147 -2.550 0.974 -2.590
wall 0.957 -2.570 0.991 -1.730
wall 0.974 -1.750 0.147 -1.650
