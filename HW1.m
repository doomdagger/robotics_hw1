% Deprecated!!!! see new file named hw1_team_16.m

function  HW1(serPort)

  % Variable Declaration
  tStart= tic;                                          % Time limit marker
  maxDuration = 600;                                    % 20 seconds of max duration time    
  currentEvent = 0;                                     % Events include '0 - init' '1 - walk' '2 - done'  
  losingWall = 0;
  initialStep = 1;
  
  while toc(tStart) < maxDuration
      [ BumpRight, BumpLeft, BumpFront, WallSensor] = AllSensorsReadRoomba(serPort); % Read Bumpers
      %display(BumpLeft)                                % Display Left Bumper Value
      %display(BumpRight)                               % Display Right Bumper Value
      %display(BumpFront)                               % Display Front Bumper Value
      %display(WallSensor)                              % Display WallSensor Value
      
      Bumped = BumpRight || BumpLeft || BumpFront;
      
      if(currentEvent == 0)
          if(Bumped) % first hit the wall, turn into event #2
              SetFwdVelRadiusRoomba(serPort, 0, inf);
              [fX, fY] = OverheadLocalizationCreate(serPort);
              currentEvent = 1;
          else
            SetFwdVelRadiusRoomba(serPort, 0.4, inf);    % Move forward until hit the wall
          end
      elseif(currentEvent == 1)
          [cX, cY] = OverheadLocalizationCreate(serPort);
          display([fX fY cX cY]);
          if(abs(cX - fX) < 0.09 && abs(cY - fY) < 0.09) 
              if(initialStep == 0)
                  currentEvent = 2;
                  continue;
              end
          else
              if(initialStep == 1)
                  initialStep = 0;
              end
          end
                  
          if(WallSensor && ~Bumped)
              %display('only wall sensor, go direct');
              losingWall = 0;
              SetFwdVelRadiusRoomba(serPort, 0.4, inf);
          elseif(Bumped)
              %display('bumped! do we have wall sensor now?');
              %very special, bumped left
              if(WallSensor)
                %display('yes, turn left');
                %turnAngle(serPort, 0.2, 3)                   % Turn Left 90 degrees
                %SetFwdVelRadiusRoomba(serPort, 0, inf);         % Stop
                SetFwdVelRadiusRoomba(serPort,0.1,0.1);
              else
                %display('no, turn dedpends...');
                if(BumpLeft)
                  turnAngle(serPort, 0.2, 110);
                  SetFwdVelRadiusRoomba(serPort, 0, inf);         % Stop
                elseif(BumpFront)                                     % If the iRobot Create Bumped on the Right
                  turnAngle(serPort, 0.2, 75)                  % Turn Right 90 degrees
                  SetFwdVelRadiusRoomba(serPort, 0, inf);         % Stop
                elseif(BumpRight)                                 % Else if the iRobot Create Bumped on the Front
                  SetFwdVelRadiusRoomba(serPort,0.15,0.2);
                end
              end
          else
              %display('we lose wall...');
              if(losingWall < 3)
                 losingWall = losingWall + 1;
                 SetFwdVelRadiusRoomba(serPort,0.18,inf);
              else
                 SetFwdVelRadiusRoomba(serPort,0.15,-0.15);
              end
          end
    
      elseif(currentEvent == 2)
          SetFwdVelRadiusRoomba(serPort, 0, inf);
          break;
      else
          break;                                        % Invalid event type, should terminate the program
      end
      pause(0.1);
  end  
  SetFwdVelRadiusRoomba(serPort, 0, inf);                                       % Stop the Robot

end