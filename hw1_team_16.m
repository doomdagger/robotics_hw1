%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMS W4733 Computational Aspects of Robotics 2015
%
% Homework 1
%
% Team number: 16
% Team leader: Ruilin Xu (rx2132)
% Team members: Qi Wang (qw2197), Haoxiang Xu (hx2185), He Li (hl2918)
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function  hw1_team_16(serPort)

    %==========================================================================
    % Variable Declaration
    %==========================================================================
    tStart= tic;                                        % Time limit marker
    maxDuration = 600;                                  % 600 seconds of max duration time    
    cur_distance = 0;                                   % Initialize Current Distance
    cur_angle = 0;                                      % Initialize Current Angle
    
    state = 'INITIAL';                                  % State of the robot: 'INITIAL', 'RUNNING', 'DONE'
    ini_x = 0;                                          % Initial Position, x-coordinate
    ini_y = 0;                                          % Initial Position, y-coordinate
    cur_x = 0;
    cur_y = 0;
    position_tolerance = 0.12;                           % Tolerance value when robot gets back to initial position
    position_firsttime = true;                          % Whether the robot reaches the initial position for the first time
    
    v_move_straight = 0.2;                              % Velocity of the robot when moving straight
    v_move_angle = 0.15;                                % Velocity of the robot when moving at an angle
    v_turn = 0.2;                                       % Speed of turning an angle
    
    no_bump_no_wall_firsttime = true;                   % Whether we lost the wall sensor and all bump sensors for the first time
    %==========================================================================
    
    %==========================================================================
    % Helper Function Declaration
    %==========================================================================
    % Check if the robot is at 's' state
    function output = isState(s)
        output = strcmp(state, s);
    end
    
    % Set robot state to 's'
    function setState(s)
        state = s;
    end

    % moveAtAngle() moves the robot at 'angle' degrees at velocity 'v' and saves the total distance
    % Note: parameter 'v' is optional
    function moveAtAngle(radius, v)
        if (nargin < 2)
            v = 0.1;
        end
        
        % Move robot
        SetFwdVelRadiusRoomba(serPort, v, radius);
        pause(0.1);
        
        % Save current position and convert current position to the
        % original x-y coordinate system
        cur_distance = DistanceSensorRoomba(serPort);
        prev_angle = cur_angle;
        turned_angle = prev_angle + cur_distance / (2 * radius);
        cord_len = 2 * radius * sin(cur_distance / (2 * radius));
        cur_x = cur_x + cord_len * cos(turned_angle);
        cur_y = cur_y + cord_len * sin(turned_angle);
        cur_angle = cur_angle + AngleSensorRoomba(serPort);
        display(sprintf('moveAtAngle\ncur_distance: %g\ncur_angle: %g\ncur_pos: [%g, %g]\n', cur_distance, cur_angle, cur_x, cur_y));
    end

    % moveForward(v) moves the robot straight ahead at velocity 'v' and saves the total distance
    % Note: parameter 'v' is optional
    function moveForward(v)
        if (nargin < 1)
            v = 0.1;
        end
        
        % Reset no_bump_no_wall_firsttime
        no_bump_no_wall_firsttime = true;
        
        % Move robot
        SetFwdVelRadiusRoomba(serPort, v, inf);
        pause(0.1);
        
        % Save current position and convert current position to the
        % original x-y coordinate system
        cur_distance = DistanceSensorRoomba(serPort);
        cur_angle = cur_angle + AngleSensorRoomba(serPort);
        cur_x = cur_x + cur_distance * cos(cur_angle);
        cur_y = cur_y + cur_distance * sin(cur_angle);
        display(sprintf('moveForward\ncur_distance: %g\ncur_angle: %g\ncur_pos: [%g, %g]\n', cur_distance, cur_angle, cur_x, cur_y));
    end

    % turnAtAngle(angle, speed) turns the robot at 'angle' degrees at speed 'speed'
    % Note: parameter 'speed' is optional
    function turnAtAngle(angle, speed)
        if (nargin < 2)
            speed = 0.1;
        end
        
        turnAngle(serPort, speed, angle);
        SetFwdVelRadiusRoomba(serPort, 0, inf);
        pause(0.1);
        
        % Save current position and convert current position to the
        % original x-y coordinate system
        cur_distance = DistanceSensorRoomba(serPort);
        cur_angle = cur_angle + AngleSensorRoomba(serPort);
        cur_x = cur_x + cur_distance * cos(cur_angle);
        cur_y = cur_y + cur_distance * sin(cur_angle);
        display(sprintf('turnAtAngle\ncur_distance: %g\ncur_angle: %g\ncur_pos: [%g, %g]\n', cur_distance, cur_angle, cur_x, cur_y));
    end

    % saveInitialPosition() saves the initial position where the robot hits the wall for the first time
    function saveInitialPosition()
        if (isState('INITIAL'))
            % Save current position as the starting position
            ini_x = cur_x;
            ini_y = cur_y;
            display(sprintf('Initial Position: [%g, %g]', ini_x, ini_y))
            setState('RUNNING');
        end
    end

    % checkTermination() decides whether the robot reaches its initial position
    function output = checkTermination()
        % Check whether the termination conditions are met
        if (abs(cur_x-ini_x) <= position_tolerance && abs(cur_y-ini_y) <= position_tolerance)
            if (~position_firsttime)
                display('Reached the initial position! STOP!')
                setState('DONE');
                output = true;
            else
                output = false;
            end
        else
            position_firsttime = false;
            output = false;
        end
    end

    % hitTheWall() lets the robot move until it hits the wall for the first time
    function hitTheWall()
        moveForward(v_move_straight);
        if (Bump)
            saveInitialPosition();
        end
    end

    % walkAlongTheWall() lets the robot move along the wall
    function walkAlongTheWall()
        if (Bump)
            if (WallSensor)
                turnAtAngle(3, v_turn);
            else
                if (BumpLeft)
                    turnAtAngle(130, v_turn);
                elseif (BumpFront)
                    turnAtAngle(70, v_turn);
                elseif (BumpRight)
                    moveAtAngle(0.15, v_move_angle);
                end
            end
        else
            if (WallSensor)
                moveForward(v_move_straight);
            else
                if (no_bump_no_wall_firsttime)
                    moveForward(v_move_straight);
                    no_bump_no_wall_firsttime = false;
                else
                    moveAtAngle(-0.1, v_move_angle);
                end
            end
        end
    end

    % stopRoomba() stops the robot and display the total distance travelled
    function stopRoomba()
        % Stop the Robot
        SetFwdVelRadiusRoomba(serPort, 0, inf);
    end
    %==========================================================================
    
    %==========================================================================
    % Main Work
    %==========================================================================
    while toc(tStart) < maxDuration
        % Get all data we need
        [BumpRight, BumpLeft, ~, ~, ~, BumpFront] = BumpsWheelDropsSensorsRoomba(serPort);
        WallSensor = WallSensorReadRoomba(serPort);
        Bump = BumpRight || BumpLeft || BumpFront;
        
        % All situations
        if (isState('INITIAL'))
            hitTheWall();
        elseif (isState('RUNNING'))
            if (checkTermination())
                continue; 
            end
            walkAlongTheWall();
        elseif (isState('DONE'))
            stopRoomba();
            break;
        end
    end
    %==========================================================================
    
end